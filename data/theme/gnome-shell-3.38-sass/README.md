# GNOME Shell Sass
GNOME Shell Sass is a project intended to allow the sharing of the
theme sources in sass between gnome-shell-3.38 and other projects like
gnome-shell-3.38-extensions.

Any changes should be done in the [GNOME Shell subtree][shell-subtree]
and not the stand-alone [gnome-shell-3.38-sass repository][sass-repo]. They
will then be synchronized periodically before releases.

## License
GNOME Shell Sass is distributed under the terms of the GNU General Public
License, version 2 or later. See the [COPYING][license] file for details.

[shell-subtree]: https://gitlab.gnome.org/GNOME/gnome-shell-3.38/tree/master/data/theme/gnome-shell-3.38-sass
[sass-repo]: https://gitlab.gnome.org/GNOME/gnome-shell-3.38-sass
[license]: COPYING
